import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Operators in Java

        //Arithmetic: +, -, *, /, %
        //Comparison: >, <. >=, <=, ==, !=
        //Logical: &&, ||, !
        //Assignment: =

        //Conditional Control Structures in Java
        //Statements allows us to manipulate the flow of the code depending on the evaluation of the condition

        //if(condition){}

        int num1 = 15;

        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        }

        //else statement will allow us to run a task if the if condition fails or receives a falsy value

        num1 = 36;

        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        /* Mini Activity */

        Scanner numScanner = new Scanner(System.in);

        /*System.out.println("Enter a number:");
        num1 = numScanner.nextInt();

        if(num1 % 2 == 0){
            System.out.println(num1 + " is even!");
        } else {
            System.out.println(num1 + " is odd!");
        }*/

        /*
        Switch Cases
            Switch statements ae control flow structures that allow one code block to be run out of many other code blocks.
            We compare the given value against each cases and if a case matches, we will run that code block.
        */
        /*System.out.println("Enter a number from 1-4:");
        int directionValue = numScanner.nextInt();

        switch(directionValue) {
            case 1:
                System.out.println("SM North EDSA");
                break;
            case 2:
                System.out.println("SM Southmall");
                break;
            case 3:
                System.out.println("SM City Tagaytay");
                break;
            case 4:
                System.out.println("SM Manila");
                break;
            default:
                System.out.println("Out of Range");
        }*/

        //Arrays
        //Arrays are objects that can contain data. However, in Java, arrays have a fixed/limited number of values with the same data type.
        //Unlike in JS, the length of Java arrays are established when the array is created
        //Syntax:
        //dataType[] identifier = new dataType[number of elements];
        //dataType[] identifier = {elementA, elementB, ...};

        String[] newArr = new String[3];
        newArr[0] = "Clark";
        newArr[1] = "Bruce";
        newArr[2] = "Diana";
        //newArr[3] = "Barry"; - out of bounds
        //newArr[2] = 25; - incorrect data type

        //This will show the memory address of our array or the location of our array within the memory:
        System.out.println(newArr);

        //To display the actual values of the array, we have to convert it to a string using the Arrays class from Java.
        //toString() is used to show the values of the array as a string in the terminal.
        System.out.println(Arrays.toString(newArr));

        //Array Methods
        //toString() - retrieves the actual value of the array as a string

        //Sort Method
        Arrays.sort(newArr);
        System.out.println("Result of the Arrays.sort():");
        System.out.println(Arrays.toString(newArr));

        Integer[] intArr = new Integer[3];
        intArr[0] = 10;
        intArr[1] = 100;
        intArr[2] = 54;

        System.out.println("Initial order of the intArr:");
        System.out.println(Arrays.toString(intArr));

        Arrays.sort(intArr);
        System.out.println("Result of the Arrays.sort():");
        System.out.println(Arrays.toString(intArr));

        //binarySearch() method - allows us to pass an argument/item to search for within our array. binarySearch() wil then return the index number of the found element.
        //You can use scanner to get input for your search term.
        String searchTerm = "Bruce";
        int result = Arrays.binarySearch(newArr, searchTerm);
        System.out.println("The index of " + searchTerm + " is: " + result);

        //Array with initialized values:
        String[] arrSample = {"Tony", "Thor", "Steve"};
        System.out.println(Arrays.toString(arrSample));

        /*
        ArrayList
                Array Lists are resizable collections/arrays that function similarly to how arrays work in JS.
                Using the new keyword in creating an ArrayList does not require the dataType for the array list to be defined to avoid repetition.
         */
        //Syntax:
        //ArrayList<dataType> identifier = new ArrayList<>();

        ArrayList<String> students = new ArrayList<>();

        //ArrayList Methods
        //arrayListName.add(<itemToAdd>) - adds elements in our array list:
        students.add("Paul");
        students.add("John");
        System.out.println(students);

        //arrayListName.get(index) - retrieve items from the array list using its index
        System.out.println(students.get(1));

        //arrayListName.set(index, value) - update an item by its index
        students.set(0, "George");
        System.out.println(students);

        //arrayListName.remove(index) - remove an item from the array list based on its index
        students.remove(1);
        System.out.println(students);

        students.add("Ringo");

        //arrayListName.clear() - clears out items in the array list
        students.clear();
        System.out.println(students);

        //arrayListName.size() - gets the length of the array list
        System.out.println(students.size());

        students.add("James");
        students.add("Wade");
        students.add("Bosh");
        System.out.println(students.size());

        //ArrayList with initialized values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates", "Elon Musk", "Jeff Bezos"));
        System.out.println(employees);
        employees.add("Lucio Tan");
        System.out.println(employees);

        //HashMaps
        //Most objects in Java are defined and are instantiations of Classes that contain a proper set of properties and methods. There might be use cases where this is not appropriate or you may simply want to store a collection of data in key-value pairs.
        //In Java, "keys" are also referred as "fields"
        //HashMaps offers flexibility when storing a collection of data
        //Syntax
        //HashMaps<fieldDataType, valueDataType> identifier = new HashMap<>();
        HashMap<String, String> userRoles = new HashMap<>();

        //Add new field and values in the hashmap
        //hashMapName.put(<field>,<value>);
        userRoles.put("Anna", "Admin");
        userRoles.put("Alice", "User");
        System.out.println(userRoles);
        userRoles.put("Alice", "Teacher");
        System.out.println(userRoles);
        userRoles.put("Dennis", "User");
        System.out.println(userRoles);

        //Retrieve values by field
        //hashMapName.get("field");
        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Dennis"));
        System.out.println(userRoles.get("Anna"));
        System.out.println(userRoles.get("anna"));

        //Remove an element/field-value pair
        //hashMapName.remove("field");
        userRoles.remove("Dennis");
        System.out.println(userRoles);

        //Retrieve hashMap keys
        //hashMapName.keySet();
        System.out.println(userRoles.keySet());
    }
}