import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //1-4
        String[] fruitsArr = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println(Arrays.toString(fruitsArr));

        Scanner inputScanner = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of?");
        String fruit = inputScanner.nextLine();
        System.out.println("The index of " + fruit + " is: " + Arrays.binarySearch(fruitsArr, fruit));

        //5-6
        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friends);

        //7-8
        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}